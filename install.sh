#!/bin/bash

# Google Chrome
wget -qO - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo gpg --dearmor -o $KEYRINGS_PATH/google-keyring.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=$KEYRINGS_PATH/google-keyring.gpg] \
    http://dl.google.com/linux/chrome/deb/ stable main" \
    | sudo tee /etc/apt/sources.list.d/google.list

# Update Repositories
sudo apt-get update

# Install Google Chrome
sudo apt-get install -y google-chrome-stable